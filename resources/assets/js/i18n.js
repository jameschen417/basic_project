/**
 * i18n
 */
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from '../../lang/i18n/en.js';
import zh_tw from '../../lang/i18n/zh_tw.js';
Vue.use(VueI18n);
const i18n = new VueI18n({
    locale: 'zh_tw',
    messages: {
        en,
        zh_tw
    },
});

export default i18n;
