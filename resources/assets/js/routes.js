import Vue from "vue";
import Router from "vue-router";

// create loan mode
import Home from './components/Home';
import Example from './components/example/Index';

Vue.use(Router);

const routes = new Router({
    history: true,
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home,
            children: [
                {
                    path: '/example',
                    name: 'example',
                    component: Example
                },
            ],
        },
    ],
});

export default routes;
