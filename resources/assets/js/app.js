
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

/**
 * imports
 */
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import i18n from './i18n.js';
import router from './routes.js';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store from'./store/index.js';
import EvaIcons from 'vue-eva-icons';
import locale from 'element-ui/lib/locale/lang/zh-TW'
import animated from 'animate.css'

//default axios api route
axios.defaults.baseURL = '/api';

Vue.use(ElementUI, {locale});
Vue.use(VueAxios, axios);
Vue.use(EvaIcons);
Vue.use(animated);
Vue.component('MyApp', require('./components/MyApp.vue'));
// Vue.diretive('focus', {
//     inserted: function(el) {
//         el.focus();
//     }
// });


/**
 *
 * @type {Vue | CombinedVueInstance<Vue, object, object, object, Record<never, any>>}
 */
const app = new Vue({
    el: '#app',
    router,
    store,
    i18n
});
