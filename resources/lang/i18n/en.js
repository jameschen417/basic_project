export default {
    //default parameters
    project_name: 'Loan Management',
    system_user: 'System User',
    first_side_bar: 'Host List',
    second_side_bar: 'Loan List',
    login_account: 'Account',
    login_account_tag: 'please enter your account',
    login_password: 'Password',
    login_password_tag: 'please enter your password',
    login: 'Login',
    logout: 'Logout',
    confirm: 'Confirm',
    cancel: 'Cancel',
    delete: 'Delete',
    //project custom parameters
}
