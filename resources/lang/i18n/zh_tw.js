export default {
    //default parameters
    project_name: '利息貸管理系統',
    system_user: '當前登入系統使用者',
    first_side_bar: '聽主列表',
    second_side_bar: '貸款總表',
    login_account: '帳號',
    login_account_tag: '請輸入會員帳號',
    login_password: '密碼',
    login_password_tag: '請輸入會員密碼',
    login: '登入',
    logout: '登出',
    confirm: '確認',
    cancel: '取消',
    delete: '刪除',
    //project custom parameters
}
