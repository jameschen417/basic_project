<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    | 中文翻譯參考 https://laravel.tw/docs/5.2/validation
    */

    'accepted'             => ':attribute 的值必須為同意(yes、on、1、或 true)。',
    'active_url'           => ':attribute 必須為有效的網址 。',
    'after'                => ':attribute 必須為在 :date 之後的日期。',
    'after_or_equal'       => ':attribute 必須為在 :date 之後或相等的日期。',
    'alpha'                => ':attribute 僅可包含字母字元。',
    'alpha_dash'           => ':attribute 僅可包含字母、數字、破折號以及底線。',
    'alpha_num'            => ':attribute 僅可包含字母、數字。',
    'array'                => ':attribute 必須是一個陣列。',
    'before'               => ':attribute 必須為在 :date 之前的日期。',
    'before_or_equal'      => ':attribute 必須為在 :date 之前或相等的日期。',
    'between'              => [
        'numeric' => ':attribute 值的大小必須介於指定的 :min 和 :max 之間。',
        'file'    => ':attribute 檔案大小必須介於 :min 到 :max KB 之間。',
        'string'  => ':attribute 字串必須介於 :min 到 :max 個字元之間。',
        'array'   => ':attribute 陣列必須有 :min 到 :max 個元素之間。',
    ],
    'boolean'              => ':attribute 欄位值必須要能夠轉型為布林值。',
    'confirmed'            => ':attribute 欄位值必須和 :attribute_confirmation 命名型式的欄位其值一致。',
    'date'                 => ':attribute 必須是有效的日期。',
    'date_format'          => ':attribute 不符合 :format 定義的日期格式。',
    'different'            => ':attribute 欄位值與 :other 欄位值必須不同。',
    'digits'               => ':attribute 必須是長度為 :digits 的數字。',
    'digits_between'       => ':attribute 必須是長度為 :min 到 :max 之間的數字。',
    'dimensions'           => ':attribute 有無效的圖片尺寸(長、寬、比例)。',
    'distinct'             => ':attribute 欄位值有重複的值。',
    'email'                => ':attribute 必須為合法的 email 格式。',
    'exists'               => ':attribute 不存在於指定的資料表中。',
    'file'                 => ':attribute 必須為一個文件。',
    'filled'               => ':attribute 欄位必須要有一個值。',
    'image'                => ':attribute 欄位檔案必須為圖片格式。',
    'in'                   => ':attribute 欄位值須存在給定的清單裡。',
    'in_array'             => ':attribute 欄位值不存在 :other 之中。',
    'integer'              => ':attribute 必須是整數。',
    'ip'                   => ':attribute 必須為合法的 IP 位址格式。',
    'ipv4'                 => ':attribute 必須為合法的 IPv4 位址格式。',
    'ipv6'                 => ':attribute 必須為合法的 IPv6 位址格式',
    'json'                 => ':attribute 必須為合法的 JSON 字串。',
    'max'                  => [
        'numeric' => ':attribute 欄位值不可以大於 :max。',
        'file'    => ':attribute 檔案大小不可以大於 :max KB。',
        'string'  => ':attribute 字串不可以大於 :max 個字元。',
        'array'   => ':attribute 陣列不可以多於 :max 個元素。',
    ],
    'mimes'                => ':attribute 欄位檔案的 MIME 類型 (副檔名) 必須在 :values 清單裡。',
    'mimetypes'            => ':attribute 欄位檔案的 MIME 類型 (檔案標頭) 必須在 :values 清單裡。',
    'min'                  => [
        'numeric' => ':attribute 欄位值不可以小於 :min。',
        'file'    => ':attribute 檔案大小不可以小於 :min KB。',
        'string'  => ':attribute 字串不可以小於 :min 個字元。',
        'array'   => ':attribute 陣列不可以少於 :min 個元素。',
    ],
    'not_in'               => ':attribute 的欄位值必須在給定的清單裡。',
    'numeric'              => ':attribute 欄位值必須為數值',
    'present'              => ':attribute 欄位值必須存在。',
    'regex'                => ':attribute 欄位值符合給定的正規表示式。',
    'required'             => '輸入資料裏必須有此 :attribute 欄位，且此欄位必填。',
    'required_if'          => '當 :other 等於 :values 時， :attribute 欄位必填。',
    'required_unless'      => '當 :other 不等於 :values 時， :attribute 欄位必填。',
    'required_with'        => '當 :values 欄位任一個存在時， :attribute 欄位必填。',
    'required_with_all'    => '當 :values 所有欄位都存在時， :attribute 欄位必填。',
    'required_without'     => '當 :values 欄位任一個不存在時， :attribute 欄位必填。',
    'required_without_all' => '當 :values 所有欄位都不存在時， :attribute 欄位必填。',
    'same'                 => ':attribute 欄位值必須和 :other 欄位值相同。',
    'size'                 => [
        'numeric' => ':attribute 必須為 :size.',
        'file'    => ':attribute 必須為 :size KB。',
        'string'  => ':attribute 必須為 :size 個字元。.',
        'array'   => ':attribute 必須包含 :size 個元素。',
    ],
    'string'               => ':attribute 必須為字串。',
    'timezone'             => ':attribute 必須為有效時區。',
    'unique'               => ':attribute 在資料表中已經存在。',
    'uploaded'             => ':attribute 檔案上傳失敗。',
    'url'                  => ':attribute 必須符合 URL 格式。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
