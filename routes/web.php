<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

/**
 * GCP Ingress Health Check
 */
Route::get('/healthz', function () {
    return response('OK', 200);
});

/**
 * vue-router definition
 */
Route::get('{all}', function() {
    return view('index');
})->where('all', '.*');
