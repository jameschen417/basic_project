<?php

use Illuminate\Database\Seeder;
use App\Management\Permission\Service as PermissionService;
use App\Management\RolePermission\Service as RolePermissionService;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    /********* 操作手冊 ********
     *    將所有要更新或新增的資料加入 $new_permission_list，或從 $new_permission_list 移除
     *   ** Warning: Tags 作為辨識 permission entities 的唯一值，不可以修改
     *
     *   1. 刪除權限請直接從 $new_permission_list 中移除即可 程式會先撈取資料庫中的 tags 比對 new_permission_list 中的 tags，若是沒有發現 tags 則"硬"刪除資料庫中的缺少 tags 的 permission 及 role_permission 欄位
     *   2. 程式再以updateOrCreate 搜尋 tags，若資料庫存在，則更新 permission entities，若是 更新 id ，連帶role_permission 的 permission_id 也作更新 (onUpdate('cascade'))
     *    若資料庫中不存在此 tag，則新增 permission entities
     *  3. 更新前會作 id, parent id 及無環樹檢查， 任何更新失敗都會被 rollback
     *
     * */

        try {
            $new_permission_list = array(
                ['search_data' =>['tag' => 'parameter_setting'],
                    'update_data' => ['id' => 1, 'name' => '參數設定', 'parent_id' => null, 'class' => 'search']],
                ['search_data' =>['tag' => 'division'],
                    'update_data' => ['id' => 2, 'name' => '單位設定', 'parent_id' => 1, 'class' => 'search,create,update,delete']],
                ['search_data' =>['tag' => 'cron'],
                    'update_data' => ['id' => 3, 'name' => '時點設定', 'parent_id' => 1, 'class' => 'search,update']],
                ['search_data' =>['tag' => 'system'],
                    'update_data' => ['id' => 4, 'name' => '所屬系統', 'parent_id' => 1, 'class' => 'search,create,update,delete']],
                ['search_data' =>['tag' => 'annual_maintenance'],
                    'update_data' => ['id' => 5, 'name' => '年度維護排程', 'parent_id' => 1, 'class' => 'search,create,update']],

                ['search_data' =>['tag' => 'permission'],
                    'update_data' => ['id' => 6, 'name' => '權限控管', 'parent_id' => null, 'class' => 'search']],
                ['search_data' =>['tag' => 'role_permission'],
                    'update_data' => ['id' => 7, 'name' => '角色權限控管', 'parent_id' => 6, 'class' => 'search,create,update,delete']],
                ['search_data' =>['tag' => 'account'],
                    'update_data' => ['id' => 8, 'name' => '帳號設定', 'parent_id' => 6, 'class' => 'search,create,update,delete']],

                ['search_data' =>['tag' => 'order'],
                    'update_data' => ['id' => 9, 'name' => '需求單管理', 'parent_id' => null, 'class' => 'search']],
                ['search_data' =>['tag' => 'order_detail'],
                    'update_data' => ['id' => 10, 'name' => '需求單明細', 'parent_id' => 9, 'class' => 'search,create,update,delete,lock']],
                ['search_data' =>['tag' => 'order_review'],
                    'update_data' => ['id' => 11, 'name' => '審核清單', 'parent_id' => 9, 'class' => 'search,review']],
                ['search_data' =>['tag' => 'history_order_detail'],
                    'update_data' => ['id' => 12, 'name' => '歷史需求單明細', 'parent_id' => 9, 'class' => 'search']],
                ['search_data' =>['tag' => 'history_order_review'],
                    'update_data' => ['id' => 13, 'name' => '歷史審核清單', 'parent_id' => 9, 'class' => 'search,review']],

                ['search_data' =>['tag' => 'schedule'],
                    'update_data' => ['id' => 14, 'name' => '排程表管理', 'parent_id' => null, 'class' => 'search']],
                ['search_data' =>['tag' => 'maintenance_schedule'],
                    'update_data' => ['id' => 15, 'name' => '維護排程表', 'parent_id' => 14, 'class' => 'search,update']],
                ['search_data' =>['tag' => 'gantt_chart'],
                    'update_data' => ['id' => 16, 'name' => '甘特圖', 'parent_id' => 14, 'class' => 'search']],
                ['search_data' =>['tag' => 'history_order_schedule'],
                    'update_data' => ['id' => 17, 'name' => '歷史排程表', 'parent_id' => 14, 'class' => 'search']],
                ['search_data' =>['tag' => 'history_gantt_chart'],
                    'update_data' => ['id' => 18, 'name' => '歷史甘特圖', 'parent_id' => 14, 'class' => 'search']],
                ['search_data' =>['tag' => 'log'],
                    'update_data' => ['id' => 19, 'name' => '異動紀錄', 'parent_id' => null, 'class' => 'search']],

                ['search_data' =>['tag' => 'template'],
                    'update_data' => ['id' => 20, 'name' => '範本設定', 'parent_id' => 1, 'class' => 'search,create,update,delete']]

            );
            /**
             *      第一階段：驗證 Id 及 parent id 正確性，再依照 id, parent id 結構，計算森林轉二元樹。
             * */
            // Check if all ids and parent ids are valid. If not, reject the update.
            $err_msg = $this->updateIdValidation($new_permission_list);
            if($err_msg !== "") {
                throw new \Exception($err_msg);
            }

            // Calculate left and right id and check if there is no cycle. If not, reject the update.
            $service = new PermissionService();
            $result = $service->constructNewBinaryTree($new_permission_list);
            if($result !== true) {
                throw new \Exception($result);
            }
            /**
             *      第二階段：建構 ID maps，執行拓樸排序找出 安全更新順序
             * */
            //依據資料庫中原有 id，與 new_permission_list 中新id 建構新舊 id maps
            $id_maps = $service->produceIdMapsWithOldIdList($new_permission_list);

            // 執行拓樸排序找出更新順序 for $update_new_ids
            list($delete_old_ids, $update_new_ids, $create_new_ids, $deadlock_stack) = $service->findUpdateOrderByTopologicalSort($id_maps);

            /**
             *      第三階段：LOCK permissions , role_permissions，更新permissions cascade 更新 role_permissions中 id
             * */
            // lock the permissions and role_permissions tables to prevent from reading or writing from other connections
//            Joseph-20190904 >>>
            print("Locking permissions and role_permissions".PHP_EOL);
            DB::getPdo()->exec('LOCK TABLES permissions WRITE, role_permissions WRITE');
//            Joseph-20190904 <<<

            DB::beginTransaction();

            // 先執行刪除
            $num_row_deleted = $service->hardDeleteMissedPermission($delete_old_ids);
            // 再執行更新新增，執行 updateOrCreate() 時，先 update 再 create
            $service->updateOrCreatePermission($new_permission_list, array_merge( $update_new_ids, $create_new_ids));
            //若有 cycle 再解 deadlock
            $service->updateIdAtDeadLockStack($deadlock_stack);

            /**
            *      第四階段：更新 role_permissions 中若有更新後的權限值(Value )超出permission中對應的 權限種類(Class )，則限縮Value。若有新增的權限，為每位角色插入新的空權限
            *       成功後 commit 再 unlock table， 失敗 rollback
            * */

            $role_perm_service = new RolePermissionService();
            //narrow down the permission value if it exceeds the permission class at permission table
            $role_perm_service->cascadingNarrowDownPermissionValueByClass();
            // Cascading create new entities at role_permissions table
            $role_perm_service->cascadingCreateEmptyPermissionInIdList($create_new_ids);

            DB::commit(); //<--- 實驗: commit 時，就會 unlock table
            DB::getPdo()->exec('UNLOCK TABLES');
            sprintf("%d %s been deleted.".PHP_EOL, $num_row_deleted , $num_row_deleted===0? 'row has':'rows have');

//            Joseph-20190904 >>>
            print("Unlocked permissions and role_permissions".PHP_EOL."All Changes are committed successfully".PHP_EOL);
//            Joseph-20190904 <<<
        }
        catch(\Exception $e) {
            DB::rollback();
            // unlock tables if update failed.
            print('Error: Update Permission table failed. All transactions have been rollback. '.PHP_EOL.$e->getMessage().PHP_EOL);

            // Note: use SQL - "SHOW OPEN TABLES where In_use > 0;" to check if the tables are locked
            DB::getPdo()->exec('UNLOCK TABLES');


//            Joseph-20190904 >>>
            print("Unlocked permissions and role_permissions".PHP_EOL);
//            Joseph-20190904 <<<

        }
    }


    private function updateIdValidation($new_permission_list)
    {

        // 1. Check if ids and parent ids are unsigned integer
        $root_existed = false;
        $id_err_msg = "";
        $parent_id_error_msg = "";
        $err_msg = "";
        $ids = array_map (
            function($permission) use (&$id_err_msg, &$parent_id_error_msg){

                $parent_id = $permission['update_data']['parent_id'];
                if(!is_null($parent_id) && !(is_int($parent_id) && $parent_id > 0)) {
                    $parent_id_error_msg .=  ($permission['search_data']['tag'].', ');
                }

                $id =  $permission['update_data']['id'];
                if(is_int($id) && $id > 0) {
                    return $id;
                } else {
                    $id_err_msg .=  ($permission['search_data']['tag'].', ');
                }
            },
            $new_permission_list
        );

        if($id_err_msg !== "") {
            $err_msg .= substr($id_err_msg, 0, -2).' permissions have improper id value.'.PHP_EOL;
        }

        if($parent_id_error_msg !== "") {
            $err_msg .= substr($parent_id_error_msg, 0, -2).' permissions have improper parent id value.'.PHP_EOL;
        }

        // 1.1 check if duplicate values exist in id arrays
        $duplicate_id_list = array();
        foreach(array_count_values($ids) as $val => $count) {
            if ($count > 1) {
                $duplicate_id_list[] = $val;
            }
        }

        if(count($duplicate_id_list) > 0) {
            $err_msg .= implode(',', $duplicate_id_list);
            $err_msg .= " have duplicate ids declaration.".PHP_EOL;
        }


        // 2. Check if parent ids exist in ids (avoid ghost parent id) and at least one null parent id exists (root)
        $parent_id_err_msg = "";
        foreach($new_permission_list as $permission) {
            $parent_id = $permission['update_data']['parent_id'];
            $root_existed |=  is_null($parent_id);
            if(!is_null($parent_id) && !in_array($parent_id, $ids)) {
                $parent_id_err_msg .= ($permission['search_data']['tag'].', ');
            }
        }

        if($parent_id_err_msg !== "") {
            $err_msg .= substr($parent_id_err_msg, 0, -2).' permissions\' parent ids do not point(equal) to any id.'.PHP_EOL;
        }
        $err_msg .= ($root_existed? '':'There must be at least one root permission which parent_id is null.');

        return $err_msg;
    }
}
