<?php

/*
 * SQL SET datatype shows in laravel 5.8 doc, but not laravel 5.7. Since our current laravel framework is version 5.6.39.
 * SET is not supported by schema builder, which means SET should be created as a customized column type.
 * Please refer to https://coderwall.com/p/mo1gew/custom-datatype-in-laravel-schema-builder for the implementation of SET type.
 * */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

// Joseph-19062101 >>>
// Since the customized class has been added into create_permissions_table migration and all the migration files will be pended before execution.
// Thus, no more customized class is needed to add.
// Joseph-19062101 <<<

class CreateRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Joseph-19062101 >>>
        // 由於用到 SET sql type 需要用自訂的 blueprint resolver 建立 SET column
        // register new grammar class
        DB::connection()->setSchemaGrammar(new ExtendedMySqlGrammar());
        $schema = DB::connection()->getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            return new ExtendedBlueprint($table, $callback);
        });

        /*
        |--------------------------------------------------------------------------
        | Columns
        |--------------------------------------------------------------------------
        */

        $schema->create('role_permissions', function(ExtendedBlueprint $table) {
            $table->integer('role_id')->length(10)->unsigned()->comment('角色ID');
            $table->integer('permission_id')->length(10)->unsigned()->comment('權限ID');
            $table->set('permission_value', ['create', 'search', 'update', 'delete'])->default('')->comment('角色擁有的權限值');    // 欄位集合與 permissions.permission_class 相同
            $table->integer('edited_by_user_id')->length(10)->unsigned()->comment('修改角色權限的使用者ID');
            $table->softDeletes();
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

        /*
        |--------------------------------------------------------------------------
        | Indices
        |--------------------------------------------------------------------------
        */
        $table->primary(['role_id', 'permission_id']);
        // 隨著權限 ID 的更新刪除，連動變更
        $table->foreign('permission_id')->references('id')->on('permissions')->onUpdate('cascade')->onDelete('cascade');
        // 隨著角色 ID 的刪除，連動刪除
        $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        $table->foreign('edited_by_user_id')->references('id')->on('users');

        // Since innodb index foreign key columns automatically, there is  only one more index you may need.
        $table->index(['role_id', 'permission_id']);
    });

    DB::statement("ALTER TABLE `role_permissions` comment '角色權限值'");

    // Joseph-19062101 <<<
}

/**
 * Reverse the migrations.
 *
 * @return void
 */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
    }
}
