<?php
/*
 * SQL SET datatype shows in laravel 5.8 doc, but not laravel 5.7. Since our current laravel framework is version 5.6.39.
 * SET is not supported by schema builder, which means SET should be created as a customized column type.
 * Please refer to https://coderwall.com/p/mo1gew/custom-datatype-in-laravel-schema-builder for the implementation of SET type.
 * */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// Joseph-19062101 >>>
// 新增此這一段程式碼，讓不支援 MySQL SET type 的 Laravel 5.6 得以宣告 SET type
use Illuminate\Support\Fluent;
use Illuminate\Database\Schema\Grammars\MySqlGrammar;

/**
 * Extended version of MySqlGrammar with
 * support of 'set' data type
 */
class ExtendedMySqlGrammar extends MySqlGrammar {

    /**
     * Create the column definition for an 'set' type.
     *
     * @param  \Illuminate\Support\Fluent  $column
     * @return string
     */
    protected function typeSet(Fluent $column)
    {
        return "set('".implode("', '", $column->allowed)."')";
    }
}

/**
 * Extended version of Blueprint with
 * support of 'set' data type
 */
class ExtendedBlueprint extends Blueprint {

    /**
     * Create a new 'set' column on the table.
     *
     * @param  string  $column
     * @param  array   $allowed
     * @return \Illuminate\Support\Fluent
     */
    public function set($column, array $allowed)
    {
        return $this->addColumn('set', $column, compact('allowed'));
    }

}

// Joseph-19062101 <<<


class CreatePermissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Joseph-19062101 >>>

        // register new grammar class
        DB::connection()->setSchemaGrammar(new ExtendedMySqlGrammar());
        $schema = DB::connection()->getSchemaBuilder();

        // replace blueprint
        $schema->blueprintResolver(function($table, $callback) {
            return new ExtendedBlueprint($table, $callback);
        });
        // Joseph-19062101 <<<

        /*
        |--------------------------------------------------------------------------
        | Columns
        |--------------------------------------------------------------------------
        */

        $schema->create('permissions', function(ExtendedBlueprint $table){

            $table->increments('id')->comment('系統索引');
            $table->string('name', 45)->unique()->comment('權限名稱');
            // tag 用於程式中搜尋權限用的標籤
            $table->string('tag',45)->nullable()->unique()->comment('英文名稱');

            // TODO: 依據專案所需要的權限種類自行修改
            $table->set('permission_class', ['create', 'search', 'update', 'delete'])->comment('允許的權限範圍種類');

            // left_id 及 right_id 用以解構二元樹至森林轉換
            $table->integer('left_id')->length(10)->unsigned()->nullable()->comment('指向左子樹子節點');
            $table->integer('right_id')->length(10)->unsigned()->nullable()->comment('指向右子樹兄弟節點');

            // parent_id is not used for structure retrieval. However, it may be used somewhere in the future. You are free to remove this if you don't need this column.
            $table->integer('parent_id')->length(10)->unsigned()->nullable()->comment('指向父節點');

            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();
        });

        DB::statement("ALTER TABLE `permissions` comment '權限種類'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
