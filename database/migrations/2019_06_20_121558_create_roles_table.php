<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {

            /*
           |--------------------------------------------------------------------------
           | Columns
           |--------------------------------------------------------------------------
           */
            $table->increments('id')->comment('系統索引');
            $table->string('name', 45)->comment('角色名稱');
            $table->integer('edited_by_user_id')->length(10)->unsigned()->comment('修改角色的使用者ID');
            $table->softDeletes();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            /*
            |--------------------------------------------------------------------------
            | Indices
            |--------------------------------------------------------------------------
            */
            $table->foreign('edited_by_user_id')->references('id')->on('users');

        });

        DB::statement("ALTER TABLE `roles` comment '角色'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
