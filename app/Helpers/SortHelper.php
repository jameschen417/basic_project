<?php
/**
 * Created by PhpStorm.
 * User: steven_wang
 * Date: 2018/10/25
 * Time: 下午 03:38
 */

namespace App\Helpers;


class SortHelper
{
    /**
     * @param array $data
     * @param string $sort_type
     * @param string $data_column
     * @return array
     */
    public function sortTwoDimensionalArray(array $data, string $sort_type, string $data_column)
    {
        #排序基準欄位
        $base_column = array_column($data, $data_column);
        if (strcasecmp($sort_type, 'asc') == 0) {
            array_multisort($base_column, SORT_ASC, $data);
        } else if (strcasecmp($sort_type, 'desc') == 0) {
            array_multisort($base_column, SORT_DESC, $data);
        }

        return $data;
    }
}