<?php
/**
 * Created by PhpStorm.
 * User: steven_wang
 * Date: 2018/10/25
 * Time: 下午 03:38
 */

namespace App\Helpers;

class FolderHelper
{
    public function checkDirectionExist($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir);
        }
    }
}