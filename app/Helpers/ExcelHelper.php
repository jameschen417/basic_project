<?php
/**
 * Created by PhpStorm.
 * User: steven_wang
 * Date: 2018/10/25
 * Time: 下午 03:38
 */

namespace App\Helpers;

use Maatwebsite\Excel\Facades\Excel;

class ExcelHelper
{

    /**
     * @param $dir
     * @param $data array -
     *  @@form
     *     $data = [
     *          [
     *             'file_name'    => 'file_name_1',
     *             'details'      => [
     *                     [
     *                         'label' => 'label_1',
     *                         'table_head' => ['name', 'email', 'phone'],
     *                         'table_body' => [
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                         ],
     *                     ],
     *                     [
     *                         'label' => 'label_2',
     *                         'table_head' => ['name', 'email', 'phone'],
     *                         'table_body' => [
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                     ],
     *             ],
     *          ],
     *          [
     *             'file_name'    => 'file_name_1',
     *             'details'      => [
     *                     [
     *                         'label' => 'label_1',
     *                         'table_head' => ['name', 'email', 'phone'],
     *                         'table_body' => [
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                         ],
     *                     ],
     *                     [
     *                         'label' => 'label_2',
     *                         'table_head' => ['name', 'email', 'phone'],
     *                         'table_body' => [
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                             ['Leon',  'leon@gmail.com', '0908669045'],
     *                     ],
     *             ],
     *          ],
     *      ];
     * @return mixed
     */
    public function exportCsvFile($dir, array $data)
    {
        foreach ($data as $key => $file_data) {
            #編寫資料
            Excel::create($file_data['file_name'],function ($excel) use ($file_data){
                #讀取資料內容
                foreach ($file_data['details'] as $detail_key => $detail) {
                    $cellData = [];
                    $cellData[0] = $detail['table_head'];

                    foreach ($detail['table_body'] as $body_key => $tbody) {
                        $cellData[($body_key + 1)] = $tbody;
                    }

                    $excel->sheet($detail['label'], function ($sheet) use ($cellData){
                        $sheet->rows($cellData);
                    });
                }
            })->store('xls', $dir);
        }

        return true;
    }
}