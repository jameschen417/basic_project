<?php
/**
 * Created by PhpStorm.
 * User: steven_wang
 * Date: 2018/10/25
 * Time: 下午 03:38
 */

namespace App\Helpers;

class DateHelper
{
    public function getLastPeriodDate($date_start, $date_end)
    {
        $date_diff = strtotime($date_end) - strtotime($date_start) + 24 * 60 * 60;
        $period_date_start = date("Y-m-d", strtotime($date_start) - $date_diff);
        $period_date_end = date("Y-m-d", strtotime($date_end) - $date_diff);
        return ['period_date_start' => $period_date_start, 'period_date_end' => $period_date_end, 'date_difference' => $date_diff];
    }

    public function getPeriodDate($date_start, $date_end, $period_type)
    {
        $month = date("Y-m", strtotime("last day of previous month", strtotime(date('Y-m'))));
        if ($period_type == 'last_month') {
            $month = date("Y-m", strtotime("-2 month", strtotime(date('Y-m'))));
        }
        $period_date_start = $month.'-01';
        $period_date_end = date('Y-m-t', strtotime($period_date_start));

        return ['period_date_start' => $period_date_start, 'period_date_end' => $period_date_end, 'date_difference' => 0];
    }
}