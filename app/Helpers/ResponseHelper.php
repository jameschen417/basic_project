<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/10/15
 * Time: 下午3:48
 */

namespace App\Helpers;

trait ResponseHelper
{
    public static function responseMaker($code, $message, $data = null)
    {
        //Status Code請參考
        //https://blog.gslin.org/archives/2016/05/25/6596/%E6%BC%82%E4%BA%AE%E7%9A%84-http-response-code-cheatsheet/
        // 成功請求 http_code 一律設置 200, 失敗請求 http_code 一律設置 400，作為前端 error handling 依據
        $department_code = '22';
        $project_code = '02';
        switch ($code) {
            case '201':
                $response = [
                    'http_code' => 200,
                    'status_message' => '查詢成功',
                ];
                break;
            case '202':
                $response = [
                    'http_code'=>200,
                    'status_message'=>'取得詳情資料成功',
                ];
                break;
            case '203':
                $response = [
                    'http_code'=>200,
                    'status_message'=>'偵測到未設定異動通知。',
                ];
                break;
            case '204':
                $response = [
                    'http_code'=>200,
                    'status_message'=>'偵測到已設定異動通知。',
                ];
                break;

            case '205':
                $response = [
                    'http_code'=>200,
                    'status_message'=>'取得有審核權限的角色成功。',
                ];
                break;
            case '206':
                $response = [
                    'http_code'=>200,
                    'status_message'=>'取得有審核權限的角色以及簽核流程成功。',
                ];
                break;
            case 207:
                $response = [
                    'http_code' => 200,
                    'error_code' => null,
                    'status_message' => '查無資料',
                ];
                break;
            case 208:
                $response = [
                    'http_code'=>200,
                    'status_message'=>'使用者驗證正確，登入成功。',
                ];
                break;
            case '101':
                $response = [
                    'http_code' => 200,
                    'status_message' => '新增成功',
                ];
                break;
            case '102':
                $response = [
                    'http_code' => 200,
                    'status_message' => '通知紀錄寫入成功',
                ];
                break;
            case '103':
                $response = [
                    'http_code' => 200,
                    'status_message' => '設置 SESSION 成功。',
                ];
                break;

            case '104':
                $response = [
                    'http_code' => 200,
                    'status_message' => '新增防火牆成功。',
                ];
                break;

            case '105':
                $response = [
                    'http_code' => 200,
                    'status_message' => '延時防火牆成功。',
                ];
                break;
            case 106:
                $response = [
                    'http_code' => 200,
                    'status_message' => '通過審核單成功。',
                ];
                break;
            case 107:
                $response = [
                    'http_code' => 200,
                    'status_message' => '拒絕申請單成功。',
                ];
                break;
            case '301':
                $response = [
                    'http_code' => 200,
                    'status_message' => '更新成功',
                ];
                break;
            case 302:
                $response = [
                    'http_code' => 200,
                    'status_message' => '更新登入時間成功',
                ];
                break;
            case '401':
                $response = [
                    'http_code' => 200,
                    'status_message' => '刪除成功',
                ];
                break;
            case 0:
                $response = [
                    'http_code' => 200,
                    'error_code' => null,
                    'status_message' => $message,
                ];
                break;
            case 1:
                $response = [
                    'http_code' => 400,
                    'status_message' => $message,
                ];
                break;

            case 2:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200002',
                    'status_message' => '無法找到該使用者',
                ];
                break;

            case 3:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200004',
                    'status_message' => '找無此目錄',
                ];
                break;

            case 4:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200005',
                    'status_message' => '找不到該檔案路徑',
                ];
                break;
            case 5:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200006',
                    'status_message' => '拒絕存取',
                ];
                break;
            case 6:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200006',
                    'status_message' => '請輸入API HTML路徑',
                ];
                break;

            case 7:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200007',
                    'status_message' => '檔案格式錯誤',
                ];
                break;

            case 8:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200008',
                    'status_message' => '建立資料失敗',
                ];
                break;

            case 9:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200009',
                    'status_message' => '輸入資料有誤',
                ];
                break;

            case 10:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200009',
                    'status_message' => '建立HTML檔失敗',
                ];
                break;
            case 16:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200013',
                    'status_message' => '此方法不允許存取',
                ];
                break;

            case 17:
                $response = [
                    'http_code' => 200,
                    'error_code' => null,
                    'status_message' => '查無資料',
                ];
                break;

            case 18:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200006',
                    'status_message' => '拒絕存取，請使用公司Email',
                ];
                break;
            case 501:
                $response = [
                    'http_code' => 400,
                    'status_message' => '拒絕存取，請先登入後再操作。',
                ];
                break;
            case 502:
                $response = [
                    'http_code' => 400,
                    'status_message' => '拒絕存取，CSRF TOKEN 不匹配。',
                ];
                break;
            case 503:
                $response = [
                    'http_code' => 400,
                    'status_message' => '查無此 API，請檢查網址是否錯誤。',
                ];
                break;
            case 504:
                $response = [
                    'http_code' => 400,
                    'status_message' => '拒絕存取，使用者無此操作權限。',
                ];
                break;
            case 505:
                $response = [
                    'http_code' => 400,
                    'status_message' => '用戶在時間內傳送了過多的請求，請聯絡客服回報此問題。',
                ];
                break;
            case 506:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請求錯誤，請聯絡客服。',
                ];
                break;
            case 507:
                $response = [
                    'http_code' => 400,
                    'status_message' => '系統繁忙，無法處理此請求，請稍後再試。',
                ];
                break;
            case 508:
                $response = [
                    'http_code' => 400,
                    'status_message' => '申請角色不符合系統使用角色，請重新登入後再嘗試。',
                ];
                break;
            case 601:
                $response = [
                    'http_code' => 400,
                    'status_message' => '新增失敗，請聯絡客服，請求格式錯誤: '.$message,
                ];
                break;

            case 602:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請輸入日期平日資料',
                ];
                break;

            case 603:
                $response = [
                    'http_code' => 400,
                    'status_message' => '此專案已設定過異動通知。',
                ];
                break;

            case 604:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請至少選擇一位通知人員。',
                ];
                break;

            case 605:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請選擇寄送方式。',
                ];
                break;

            case 606:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請填寫通知內容。',
                ];
                break;

            case 607:
                $response = [
                    'http_code' => 400,
                    'status_message' => '建立異動通知失敗，請聯絡客服。',
                ];
                break;

            case 608:
                $response = [
                    'http_code' => 400,
                    'status_message' => '建立通知人員失敗，請聯絡客服。',
                ];
                break;

            case 609:
                $response = [
                    'http_code' => 400,
                    'status_message' => '建立通知紀錄失敗，請聯絡客服。',
                ];
                break;

            case 610:
                $response = [
                    'http_code' => 400,
                    'status_message' => '新增費用監控設定失敗，請聯絡客服。',
                ];
                break;
            case 611:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請輸入日期。',
                ];
                break;
            case 612:
                $response = [
                    'http_code' => 400,
                    'status_message' => '請輸入節日名稱。',
                ];
                break;
            case 613:
                $response = [
                    'http_code' => 400,
                    'status_message' => '日期 '. $data .' 已設定過。',
                ];
                break;
            case 614:
                $response = [
                    'http_code' => 400,
                    'status_message' => '建立失敗，請聯絡客服。',
                ];
                break;
            case 615:
                $response = [
                    'http_code' => 400,
                    'status_message' => '建立失敗，您所嘗試建立的簽核流程已經存在，請先更新或是刪除此部門的流程。',
                ];
                break;
            case 616:
                $response = [
                    'http_code' => 400,
                    'status_message' => '設置 SESSION 失敗，請聯絡客服：'.$message.'。',
                ];
                break;
            case 617:
                $response = [
                    'http_code' => 400,
                    'status_message' => '使用者綁定角色失敗，請聯絡客服。',
                ];
                break;
            case 618:
                $response = [
                    'http_code' => 400,
                    'status_message' => '角色新增專案範圍失敗，請聯絡客服。',
                ];
                break;

            case 619:
                $response = [
                    'http_code' => 400,
                    'status_message' => '新增防火牆失敗(名單)，請聯絡客服。',
                ];
                break;

            case 620:
                $response = [
                    'http_code' => 400,
                    'status_message' => '新增防火牆失敗(細項)，請聯絡客服。',
                ];
                break;

            case 621:
                $response = [
                    'http_code' => 400,
                    'status_message' => '新增防火牆簽核單據失敗，請聯絡客服。',
                ];
                break;

            case 622:
                $response = [
                    'http_code' => 400,
                    'status_message' => '已有使用者針對此防火牆申請延時時間，請刷新頁面，再申請沿時。',
                ];
                break;

            case 623:
                $response = [
                    'http_code' => 400,
                    'status_message' => '已有使用者針對此防火牆申請相同延時時間，請刷新頁面，再申請沿時。',
                ];
                break;

            case 624:
                $response = [
                    'http_code' => 400,
                    'status_message' => '延時防火牆失敗，請聯絡客服。',
                ];
                break;

            case 701:
                $response = [
                    'http_code' => 400,
                    'status_message' => '取得異動通知詳情失敗，請聯絡客服。',
                ];
                break;
            case 702:
                $response = [
                    'http_code' => 400,
                    'status_message' => '取得通知人員失敗，請聯絡客服。',
                ];
                break;
            case 703:
                $response = [
                    'http_code' => 400,
                    'status_message' => '取得資料失敗，請聯絡客服，請求格式錯誤: '.$message,
                ];
                break;
            case 704:
                $response = [
                    'http_code' => 400,
                    'status_message' => '使用者不存在，請聯絡客服。',
                ];
                break;
            case 705:
                $response = [
                    'http_code' => 400,
                    'status_message' => '使用者驗證錯誤，登入失敗。',
                ];
                break;
            case 706:
                $response = [
                    'http_code' => 400,
                    'status_message' => '查無此使用者，登入失敗，請為此帳號綁定使用者。',
                ];
                break;
            case 707:
                $response = [
                    'http_code' => 400,
                    'status_message' => '此使用者沒有正確的部門、角色設置，請聯絡客服。',
                ];
                break;
            case 708:
                $response = [
                    'http_code' => 400,
                    'status_message' => '此防火牆申請單為直接通過，未經簽核流程，無審核歷程可瀏覽。',
                ];
                break;
            case 709:
                $response = [
                    'http_code' => 400,
                    'status_message' => '查詢失敗。'.$message,
                ];
                break;
            case 801:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新異動通知狀態失敗，請聯絡客服。',
                ];
                break;
            case 802:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新異動通知詳情失敗，請聯絡客服。',
                ];
                break;
            case 803:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新通知人員失敗，請聯絡客服。',
                ];
                break;
            case 804:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新防火牆 Firewall_Rules 條件失敗。',
                ];
                break;
            case 805:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新費用監控資料失敗，請聯絡客服。',
                ];
                break;
            case 806:
                $response = [
                    'http_code' => 400,
                    'status_message' => '此日期已設定過行事曆，請更換其他時間。',
                ];
                break;
            case 807:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新失敗，請聯絡客服。',
                ];
                break;
            case 808:
                $response = [
                    'http_code' => 400,
                    'status_message' => '停用簽核流程失敗，尚有審核中表單採用此流程，請先完成審核。',
                ];
                break;
            case 809:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新簽核流程失敗，查無此流程存在，請聯絡客服。',
                ];
                break;
            case 810:
                $response = [
                    'http_code' => 400,
                    'status_message' => '停用簽核流程失敗，此簽核流程不存在，或是已為停用或刪除狀態，此操作無導致任何流程被停用，請聯絡客服。',
                ];
                break;
            case 811:
                $response = [
                    'http_code' => 400,
                    'status_message' => '啟用簽核流程失敗，此簽核流程不存在，或是已為啟用或已被刪除狀態，此操作無導致任何流程被啟用，請聯絡客服。',
                ];
                break;
            case 812:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新失敗，請聯絡客服，請求格式錯誤: '.$message,
                ];
                break;
            case 813:
                $response = [
                    'http_code' => 400,
                    'status_message' => '更新角色設定失敗，請聯絡客服，詳細更新如下: '.$message,
                ];
                break;
            case 814:
                $response = [
                    'http_code' => 400,
                    'status_message' => '簽核審核失敗: '.$message,
                ];
                break;
            case 901:
                $response = [
                    'http_code' => 400,
                    'status_message' => '刪除異動通知設定失敗，請聯絡客服。',
                ];
                break;

            case 902:
                $response = [
                    'http_code' => 400,
                    'status_message' => '刪除費用監控資料失敗，請聯絡客服。',
                ];
                break;
            case 903:
                $response = [
                    'http_code' => 400,
                    'status_message' => '刪除行事曆設定失敗，請聯絡客服。',
                ];
                break;

            case 904:
                $response = [
                    'http_code' => 400,
                    'status_message' => '角色名稱使用於白名單管制，請先移除後再進行刪除。'.$message,
                ];
                break;
            case 905:
                $response = [
                    'http_code' => 400,
                    'status_message' => '簽核流程尚未先停用，或是為已被刪除狀態，無任何流程被刪除。',
                ];
                break;
            case 906:
                $response = [
                    'http_code' => 400,
                    'status_message' => '刪除失敗，請聯絡客服，請求格式錯誤: '.$message,
                ];
                break;
            case 907:
                $response = [
                    'http_code' => 400,
                    'status_message' => '刪除角色設定失敗，請聯絡客服。',
                ];
                break;
            case 908:
                $response = [
                    'http_code' => 400,
                    'status_message' => '角色名稱使用於簽核流程設定，請先移除後再進行刪除。'.$message,
                ];
                break;
            default:
                $response = [
                    'http_code' => 400,
                    'error_code' => '200003',
                    'status_message' => '錯誤的Request',
                ];
                break;
        }
        $response['status_code'] = $department_code.$project_code.$code;
        $response['return_data'] = $data;
        return $response;
    }
}