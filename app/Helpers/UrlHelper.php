<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/10/16
 * Time: 下午2:01
 */

namespace App\Helpers;


class UrlHelper
{
    public $rd5Url;
    public $rdmUrl;
    public $rd5Host;
    public $curlPort;
    public $curlProtocol;
    public $curlConnMultiple;
    public $publicPath;
    public $dataPath;
    public $dataRelativePath;
    public $originalPath;
    public $apiTestLogPath;
    public $apiTestLogRelativePath;
    public $userFilePath;
    public $userFileRelativePath;
    public $pamLog;

    public function __construct()
    {
        $this->publicPath = realpath(__DIR__ . '/../../public/') . '/';
        $this->pamLog = $this->publicPath . 'data/pam_log/';
//        $app_env = env('APP_ENV');
//        switch ($app_env) {
//            case 'local':
//                #測試環境
//                $this->rdmUrl = 'http://localhost/';
//                $this->rd5Host = 'api.bbin-asia.com';
//                $this->rd5Url = 'https://idoc.bbin-asia.com';
//                break;
//            case 'dev':
//                #開發機
//                $this->rdmUrl = 'http://34.80.152.162';
//                $this->rd5Host = 'rd5-api';
//                $this->rd5Url = 'http://rd5-api';
//                break;
//            case 'test':
//                #測試機環境
//                $this->rdmUrl = 'https://commission.openrdm.com';
//                $this->rd5Host = 'rd5-api';
//                $this->rd5Url = 'http://rd5-api';
//                break;
//        }
//
//        $this->curlPort = '80';
//        $this->curlProtocol = 'http';
//        $this->curlConnMultiple = true;
//
//        $this->publicPath = realpath(__DIR__ . '/../../public/') . '/';
//
//        $this->dataPath = $this->publicPath . 'data/';
//        $this->dataRelativePath = '../data/';
//
//        $this->originalPath = $this->publicPath . 'data/original/';
//
//        $this->apiTestLogPath = $this->publicPath . 'data/api_test_log/';
//        $this->apiTestLogRelativePath = '../data/api_test_log/';
//
//        $this->userFilePath = $this->publicPath . 'data/user_files/';
//        $this->userFileRelativePath = '../data/user_files/';
    }
}
