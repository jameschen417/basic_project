<?php
/**
 * Created by PhpStorm.
 * User: steven_wang
 * Date: 2018/9/12
 * Time: 上午 11:51
 */

namespace App\Helpers;

class CurlHelper
{
    private $ch;
    private $google_client;
    private $gcpTokenRepository;
    const POST = 'POST';
    const GET = 'GET';
    const DELETE = 'DELETE';
    const PATCH = 'PATCH';


    public function __construct()
    {
        $this->ch = curl_init();
    }

    public function findError($conn_err, $result)
    {
        if ($conn_err) {

            return [
                'error'             => $conn_err,
                'error_description' => 'curl fail',
            ];
        }

        $ret = json_decode($result, true);

        if ($ret === false) {
            return [
                'error'             => $result,
                'error_description' => 'json decode fail',
            ];
        }

        return $ret;
    }

    public function doGet($url, $query_string, $token = false,  $rd_border)
    {
        $url = $url . '?' . $query_string;
        $result = $this->execCurl($url, self::GET, [], $token, $rd_border);

        return $result;
    }

    public function doPost($url, $query_string, $token, $rd_border)
    {
        $result = $this->execCurl($url, self::POST, $query_string, $token, $rd_border);
        return $result;
    }

    public function doPut($url, $query_string, $token, $rd_border)
    {
        $result = $this->execCurl($url, self::PATCH, $query_string, $token, $rd_border);
        return $result;
    }

    public function doDelete($url, $query_string, $access_token, $rd_border)
    {
        $result = $this->execCurl($url, self::DELETE, $query_string, $access_token, $rd_border);
        return $result;
    }

    private function execCurl($url, $method, $query_string, $token, $rd_border)
    {
        $access_token = $this->getAccessToken($rd_border);

        if ($token === true) {
            $header = ['Content-Type: application/x-www-form-urlencoded', 'Content-Type:application/json'];
        } else {
            $header = ['Content-Type:application/json', 'Authorization:Bearer ' . $access_token];
        }

        switch ($method) {
            case 'GET':
                curl_setopt($this->ch, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($this->ch, CURLOPT_POST, true);
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, $query_string);
                break;
            case 'PATCH':

                curl_setopt($this->ch, CURLOPT_URL, $url);
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, self::PATCH);
                curl_setopt($this->ch, CURLOPT_POSTFIELDS, $query_string);
                break;
            case 'DELETE':
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($this->ch);
        $conn_err = curl_error($this->ch);
        return $this->findError($conn_err, $result);
    }

    public function getAccessToken($rd_border)
    {
        //putenv('GOOGLE_APPLICATION_CREDENTIALS='.base_path('./rdm-gcp-monitor.json'));

        if ($rd_border === true) {

            if (env('APP_ENV') === 'local' || env('APP_ENV') === 'dev') {
                putenv('GOOGLE_APPLICATION_CREDENTIALS='.base_path('rdm-common.json'));
            }

            $this->google_client->useApplicationDefaultCredentials();
            $this->google_client->setScopes(['https://www.googleapis.com/auth/cloud-platform']);
            $this->google_client->refreshTokenWithAssertion();

            return $this->google_client->getAccessToken()['access_token'];
        }

        //$data = $this->gcpTokenRepository->getGCPToken('109454601769559045394');
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.base_path('rdm-gcp-monitor.json'));


        $this->google_client->useApplicationDefaultCredentials();
        $this->google_client->setScopes(['https://www.googleapis.com/auth/cloud-platform']);
        $this->google_client->refreshTokenWithAssertion();

        return $this->google_client->getAccessToken()['access_token'];
    }
}
