<?php
namespace App\Management;

use App\Helpers\CurlHelper;

abstract class BaseService
{
    private $curlHelper;
    private $env;
    const ENABLE_FAKE_SESSION = 1;  // give the user all departments and permissions
    private $session;                   // set the session only once

    public function __construct()
    {
        $this->curlHelper = new CurlHelper();
        $this->env = config('app.env');
        //$this->session = $this->userId();
    }

    public function curlService($api_method, $curl_url, $curl_params, $token_header, $rd_border = false)
    {
        return $this->curlHelper->$api_method($curl_url, $curl_params, $token_header, $rd_border);
    }

//    public function userId()
//    {
//        // if you are running on a non-production server without login, a default user and role id session is set
//        // and all departments and whole permission are set.
//        if(self::ENABLE_FAKE_SESSION && $this->env === 'local' && !$this->session)
//        {
//            if(is_null(session()->get('department'))) {
//                $user_role = \DB::table('user_gcp_roles')->first();
//                if(!empty($user_role)) {
//                    $user_id = $user_role->user_id;
//                    $role_id = $user_role->gcp_role_id;
//                    session()->put('user_id', $user_id);            // <-- change user_id by yourself
//                    session()->put('gcp_role_id', $role_id);        // <-- change role_id by yourself
//
//                    // give this user all departments and all permissions
//
//                    $dep = \DB::table('departments')->get();
//                    $department = $dep->map(function($entity) {
//                        return ['department_id' => $entity->id, 'department_name' => $entity->name];
//                    });
//
//                    session()->put('department', $department->toArray());
//
//                    $perm = \DB::table('permissions')->get();
//                    $permission = ($perm->keyBy('tag'))->map(function ($entity) {
//                        return explode(',', $entity->permission_class);
//                        });
//
//                    session()->put('permission', $permission);
//                }
//            }
//            return $user_id ?? session()->get('user_id');
//        } else {
//            return session()->get('user_id');
//        }
//    }




}
