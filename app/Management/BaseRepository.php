<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/11/20
 * Time: 上午8:48
 */

namespace App\Management;

use Illuminate\Support\Facades\DB;

abstract class BaseRepository
{
    public function __construct()
    {

    }

    public function create($data)
    {
        return $this->query()->create($data);
    }

    public function insert($data)
    {
        return $this->query()->insert($data);
    }

    public function updateOrCreate($search_data, $update_data)
    {
        return $this->query()->updateOrCreate($search_data, $update_data);
    }

    public function getAll($where_data, $where_in_data = [])
    {
        return $this->query()->where(function ($query) use ($where_data, $where_in_data) {
            foreach ($where_data as $key => $value) {
                $query->where($key, $value);
            }
            foreach ($where_in_data as $key => $value) {
                $query->whereIn($key, $value);
            }
        })->get();
    }

    /**
     * 找出單一值
     * @param integer $id - 傳入ID
     */
    public function find($id)
    {
        return $this->query()->findOrFail($id);
    }

    /**
     * 尋找第一個符合 atttrbute 資料回傳，如果沒有則根據 attribute new 一個 instance 回傳
     *  @param  array  $attributes -Column name - value pairs
     * @return \Illuminate\Database\Eloquent\Model
     * */
    public function firstOrNew($attributes)
    {
        return $this->query()->firstOrNew($attributes);
    }


    /**
     * GET 資料庫資料
     * @param array $validator_data - 傳入所要撈取的條件陣列
     */
    public function get($validator_data = null)
    {

        if ($validator_data == null) {
            return $this->query()->get();
        } else {
            return $this->query()->where(function ($query) use ($validator_data) {
                foreach ($validator_data as $key => $value) {
                    $query->where($key, $value);
                }
            })->get();
        }
    }

    public function getWithTrashed($validator_data = null)
    {

        if ($validator_data == null) {
            return $this->query()->withTrashed()->get();
        } else {
            return $this->query()->withTrashed()->where(function ($query) use ($validator_data) {
                foreach ($validator_data as $key => $value) {
                    $query->where($key, $value);
                }
            })->get();
        }
    }

    /**
     *GET Fisrt 資料庫資料
     * @param array $validator_data - 傳入所要撈取的條件陣列
     */
    public function first($validator_data)
    {
        return $this->query()->where(function ($query) use ($validator_data) {
            foreach ($validator_data as $key => $value) {
                $query->where($key, $value);
            }
        })->first();
    }

    public function update($data, $id)
    {
        return $this->query()->where('id', $id)->update($data);
    }

    public function updateWithTrashed($data, $id)
    {
        return $this->query()->withTrashed()->where($id)->update($data);
    }

    private function query()
    {
        return call_user_func(static::MODEL . '::query');
    }

    public function delete($field, $id)
    {
        return $this->query()
            ->whereIn($field, $id)
            ->delete();
    }

    public function deleteOneData($data)
    {
        return $this->query()
            ->where($data)
            ->delete();
    }

    /**
     *  update data with many where conditions
     * @param array $update_data (associative array of (column, value))
     * @param array $where_data (array of [column, operator, value])
     * @return int $num_of_updated_entity
     * */
    public function updateWheres($update_data, $where_data)
    {
        return $this->query()->where($where_data)->update($update_data);
    }

}
