<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/11/15
 * Time: 下午5:05
 */

namespace App\Management\InAuth;

use App\Management\BaseService;
use Dapphp\Radius\Radius as RadiusClient;
use Illuminate\Support\Facades\Session;
//use App\Management\User\Repository as UserRepository;

class Service extends BaseService
{
    private $repository;
    private $client;
    protected $userRepository;
    public function __construct()
    {
        parent::__construct();
        $this->repository = new Repository();
        $this->client = new RadiusClient();
        //$this->userRepository = new UserRepository();
    }

    public function radiusAuth($request)
    {
        #防呆(user_name)
        if (!isset($request['user_name'])) {
            return 'please enter your user name';
        }
        #防呆(user_password)
        if (!isset($request['user_password'])) {
            return 'please enter your user password';
        }

        # set server, secret, and basic attributes
        $this->client
            ->setServer(env('RADIUS_SERVER')) //RADIUS server address
            ->setSecret(env('RADIUS_SECRET')) // RADIUS shared secret
//            ->setNaSIpAddress('10.0.1.2') //NAS sever address
//            ->setAttribute(32, 'login'); // NAS identifier
        ;


        # PAP authentication; returns true if successful, false otherwise
        $username = $request['user_name'];
        $password = $request['user_password'];
        $authenticated = $this->client->accessRequest($username, $password);

//        # CHAP-MD5 authentication
//        $this->client->setChapPassword($password); // set chap password
//        $authenticated = $this->client->accessRequest($username); // authenticate, don't specify pw here
//
//        # MSCHAP v1 authentication
//        $this->client->setMSChapPassword($password); // set ms chap password (uses openssl or mcrypt)
//        $authenticated = $this->client->accessRequest($username);
//
//        # EAP-MSCHAP v2 authentication
//        $authenticated = $this->client->accessRequestEapMsChapV2($username, $password);

        #in-auth login authorization in radius server
        if ($authenticated === false) {
            # false returned on failure
//            $result = sprintf(
//                "Access-Request failed with error %d (%s).\n",
//                $this->client->getErrorCode(),
//                $this->client->getErrorMessage()
//            );
            $result = [
                'result'=>'rejected',
                //Joseph-20190820 >>>
                'user'=>false,
                //Joseph-20190820 <<<

            ];
            # access request was accepted - client authenticated successfully
//            $result = "Success!  Received Access-Accept response from RADIUS server.\n";
        }
        //Joseph-20190820 >>>
        else {
            $result = [
                'result'=>'success',
            ];

            //$user_id = $this->userRepository->getUserIdByGoogleUserName($username);

//            if(is_null($user_id['user_id'])) {
//                // user_id is not found
//                $result['user'] = false;
//            } else {
//
//                Session::put('user_name', $username);
//                Session::put('user_id', $user_id['user_id']);
//                $result['user'] = true;
//            }
        }
        //Joseph-20190820 <<<
        
        return $result;
    }
}
