<?php

namespace App\Exceptions;

use App\Helpers\ResponseHelper;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    // Joseph-test >>>
    /**
     * Prepare a JSON response for the given exception.
     * This function handles only ajax exceptions.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function prepareJsonResponse($request, Exception $e)
    {
        // 針對 APP_DEBUG 為 true 時 (non-production) 環境下，框架 Exception 所輸出的錯誤訊息放到 data。
        $data = config('app.debug') ? [
            'message' => $e->getMessage(),
            'exception' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => collect($e->getTrace())->map(function ($trace) {
                return Arr::except($trace, ['args']);
            })->all()] : null;

        // 常見的 HttpException
        if($e instanceof  HttpException) {
            if ($e instanceof NotFoundHttpException) {                  // 404 not found
                return $this->responseMaker(503, null, $data);
            } else if ($e instanceof AccessDeniedHttpException) {        // authorization rejected in Form validation, 403 Forbidden
                return $this->responseMaker(504, null, $data);
            } else if ($e instanceof TooManyRequestsHttpException) {      // 請求超出 throttle middleware 設定, 429 Too Many Requests
                return $this->responseMaker(505, null, $data);
            } else if ($e instanceof BadRequestHttpException) {          // 400 Bad Request
                return $this->responseMaker(506, null, $data);
            } else if ($e instanceof ServiceUnavailableHttpException) { // 503 Service Unavailable
                return $this->responseMaker(507, null, $data);
            } else {
                return parent::prepareJsonResponse($request, $e);
            }
        }
        else {     // default response
            return parent::prepareJsonResponse($request, $e);
        }
    }

    protected function responseMaker($code, $message, $data)
    {
        $result = ResponseHelper::responseMaker($code, $message, $data);
        $http_code = $result['http_code'];
        unset($result['http_code']);
        return response()->json($result, $http_code)
            ->header('Content-Type', 'application/json');
    }
}
