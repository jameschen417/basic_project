<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/11/15
 * Time: 下午5:01
 */

namespace App\Http\Controllers\GCP\Example;

#custom use
use App\Http\Controllers\BaseForm;

class Form extends BaseForm
{
    public function rules()
    {
        /* 1. 所有的 route parameters 路徑參數 與 request body parameters 表單參數已經被加入驗證參數
         * 撰寫驗證 APIs request body 參數或是 route parameter 所需要"共同"套用的規則 (即所有 function 都套用)
         * */
        // 範例
        $default_rules = ['user_id' => 'integer|min:1'];

        /*
         * 2. 根據 function 需要的不同規則的 ，新增 case function_name 以覆寫 default rules 或添加新的規則
         * */

        // 範例
        $identifier = $this->getFunctionIdentifier();
        switch ($identifier) {
            case "index":
                $different_rules = [
                    'user_id'       =>  'required|integer|min:1',       // 覆寫 default rule
                    'department_id' =>  'required|integer|min:1',       // 新增 rule
                ];
                break;
            default:
                $different_rules = [];
                break;
        }

        return $this->appendOrOverwriteRules($default_rules, $different_rules);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
