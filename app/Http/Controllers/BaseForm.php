<?php
namespace App\Http\Controllers;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Helpers\ResponseHelper;

abstract class BaseForm extends FormRequest implements IForm
{
    // all available permission classes
    const PERMISSION = ['search','create','update','delete','turnoff', 'review'];

    public function __get($key)
    {
        return $key === "method_name" ?  $this->getFunctionIdentifier() : parent::__get($key);
    }

    /**
     * 相容舊版棄用的 permission(), rename function permission(權限名稱, 權限值) to can(權限值,權限名稱)，參數位置對調
     * @param string $method
     * @param array $parameters
     * @return bool|mixed
     */
    public function __call($method, $parameters)
    {
        if($method === 'permission') {
            return count($parameters) === 2 ? $this->can($parameters[1], $parameters[0]) : $this->can(...$parameters);
        }
        return parent::__call($method, $parameters);
    }

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        // 撈取 public/lang/zh/validation.php 的語言檔
        \App::setLocale('zh');
    }

    /**
     * 檢查當前登入使用者 session 中該權限有無指定的權限
     * @param string $tag - permission tag (權限別名)，參考 PermissionTableSeeder.php 或是 資料庫中 permissions.tag 欄位
     * @param string|array|\Closure $value - permission value (權限值)
     * 1. 多個值請傳陣列，參考 PermissionTableSeeder.php 或是 資料庫中 permissions.value 欄位
     * 2. Closure function(Form $request, ...) 方便直接在 Controller 裡面呼叫其他形式的驗證
     * @return bool has_permission
     * */
    public function can($value, $tag = null): bool
    {
        if(is_array($value)) {
            return count(array_diff($value, session('permission')[$tag] ?? [])) === 0;
        }
        
        if($value instanceof \Closure) {
            call_user_func($value, $this);
        }
         return in_array($value, session('permission')[$tag] ?? []);
     }

    public function failedValidation(Validator $validator)
    {
        $message = $this->messageListToString($validator->errors()->messages());
        $response = $this->ruleViolationResponse($message);
        $http_code = $response['http_code'];
        unset($response['http_code']);
        throw new HttpResponseException(response()->json($response, $http_code)->header('Content-Type', 'application/json'));
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     * @throws AuthorizationException
     */
    protected function failedAuthorization()
    {
        // send 403 forbidden HttpException
        throw new AuthorizationException();
    }


    private function getFunctionIdentifier()
    {
        return explode("@", $this->route()->action['controller'])[1];
    }
    private function getRequestMethod()
    {
        return $this->route()->methods();
    }

    /**
     * 針對驗證失敗的請求，根據其 request method 尋找對應 response
     * @param string $message
     * @return array $response
     * */
    public function ruleViolationResponse($message)
    {
        $request_method = strtoupper($this->getRequestMethod()[0]);
        switch($request_method)
        {
            case "GET":
            case "HEAD":
                $response_code = 703;
                break;
            case "POST":
                $response_code = 601;
                break;
            case "PUT":
            case "PATCH":
                $response_code = 812;
                break;
            case "DELETE":
                $response_code = 906;
                break;
            default:
                $response_code = 1;
        }
        return ResponseHelper::responseMaker($response_code, $message,null);
    }

    /**
     * @param array $messages
     * @return string $error_message
     * */
    private function messageListToString($messages)
    {
        $i = 1;
        return (count($messages) === 1) ? $this->messagesForAttribute(reset($messages)) : array_reduce($messages, function($carry, $item) use (&$i) {
            return $carry.sprintf('%d. %s ', $i++, $this->messagesForAttribute($item));
        } , '');
    }

    /**
     * @param array $messages
     * @return string $error_message
     * */
    private function messagesForAttribute($messages)
    {
        $i = 1;
        return (count($messages) === 1) ? $messages[0] : array_reduce($messages, function($carry, $item) use (&$i) {
            return $carry.sprintf('(%d) %s ', $i++, $item);
        }, '');
    }

    /**
     * 將 URL 中的路徑參數加入待驗證資料中 Overwrite the request form validationData with route parameters added.
     * @return array
     **/
    protected function validationData()
    {
        $data = parent::all();      // request->all 撈取所有 request body 參數
        return array_merge($this->route()->parameters, $data);      // 若是 request body 與 route parameter 欄位相同，但值不同，以 request body 為主
    }
}
