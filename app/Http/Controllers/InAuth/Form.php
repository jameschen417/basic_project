<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/11/15
 * Time: 下午5:01
 */

namespace App\Http\Controllers\GCP\InAuth;
use App\Helpers\ResponseHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class Form extends FormRequest
{
    public function rules()
    {
        return [
            'title' => 'required|unique|max:255',
            'body' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) {
        $response = ResponseHelper::responseMaker(1, $validator->errors(),null, 'original');
        unset($response['http_code']);
        throw new HttpResponseException(response()->json($response));
    }
}
