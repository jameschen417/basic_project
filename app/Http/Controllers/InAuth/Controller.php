<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/11/15
 * Time: 下午5:00
 */

namespace App\Http\Controllers\InAuth;

use Illuminate\Http\Request;
use App\Management\InAuth\Service as InAuthService;
use Illuminate\Support\Facades\Session;

class Controller extends \App\Http\Controllers\Controller
{
    private $inAuthService;

    public function __construct()
    {
        $this->inAuthService = new InAuthService();
    }

    public function index(Request $request)
    {
        try {
            $result = $this->inAuthService->radiusAuth($request);
            $response_code = ($result['result'] === 'rejected') ? 705 :  ( $result['user'] ? 208 : 706);

            // Regenerate session id after login to prevent malicious users from exploiting a session fixation attack.
            if( ($result['result'] !== 'rejected') &&  $result['user']) {
                $request->session()->regenerate();
                $result['_token'] = session()->token();
            }

            $response = $this->responseMaker($response_code, null, $result);
        } catch (\Exception $e) {
            $response = $this->responseMaker(1, $e->getMessage(), null);
        }
        return $response;
    }

    public function store(Request $request)
    {
        try {
            $response = $this->responseMaker(16, null, null);
        } catch (\Exception $e) {
            $response = $this->responseMaker(1, $e->getMessage(), null);
        }
        return $response;
    }

    public function show()
    {
        try {
            $response = $this->responseMaker(16, null, null);
        } catch (\Exception $e) {
            $response = $this->responseMaker(1, $e->getMessage(), null);
        }
        return $response;
    }

    public function update()
    {
        try {
            $response = $this->responseMaker(16, null, null);
        } catch (\Exception $e) {
            $response = $this->responseMaker(1, $e->getMessage(), null);
        }
        return $response;
    }

    public function destroy()
    {
        try {
            $response = $this->responseMaker(16, null, null);
        } catch (\Exception $e) {
            $response = $this->responseMaker(1, $e->getMessage(), null);
        }
        return $response;
    }

    public function logout()
    {
        try {
            Session::flush();
            $response = $this->responseMaker(401, null, null);
        } catch (\Exception $e) {
            $response = $this->responseMaker(1, $e->getMessage(), null);
        }
        return $response;
    }
}
