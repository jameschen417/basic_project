<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/12/6
 * Time: 上午9:25
 */

namespace Tests\InAuth;

use Tests\TestCase;
use App\Management\InAuth\Service as InAuthService;

class ServiceTest extends TestCase
{
    public function testRadiusAuth()
    {
        $inAuthService = new InAuthService();

        $request['user_name'] = 'leon_hsieh';
        $request['user_password'] = '1234850253';
        $inAuthService->radiusAuth($request);
    }
}
